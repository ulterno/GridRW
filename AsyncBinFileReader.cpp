#include "AsyncBinFileReader.h"

AsyncBinFileReader::AsyncBinFileReader(QJsonDocument config, QObject *parent)
	: QObject{parent}
{
	addController(config);
}

int AsyncBinFileReader::binaryDataQuery(ObjectIdType oid)
{
	for (int iter = 0; iter < listDcu.length(); iter++)
	{
		if (!listDcu[iter].ctrlr->busy && !listDcu[iter].inUse)
		{
			emit getBinaryData(iter, oid);
		}
	}
	return 1;
}

void AsyncBinFileReader::receiveFileMetadata(QJsonDocument metadata, bool success, int workerSeqNumber)
{
	Q_UNUSED (metadata);
	Q_UNUSED (success);
	Q_UNUSED (workerSeqNumber);
}

void AsyncBinFileReader::receiveBinaryData(QByteArray data, bool over, int workerSeqNumber)
{
	if (listDcu.length() > workerSeqNumber)
	{
		listDcu[workerSeqNumber].data->append(data);
		if (over)
		{
			listDcu[workerSeqNumber].inUse = false;
			QSharedPointer<QBuffer> aBuffer (new QBuffer (listDcu[workerSeqNumber].data));
			emit dataReady(aBuffer, workerSeqNumber);
		}
	}
}

int AsyncBinFileReader::addController(const QJsonDocument &cfg)
{
	GridFileSystemController::DB_CONFIG config{};
	config.hostName = cfg["host"].toString();
	config.portNo = cfg["port"].toInt();
	config.username = cfg["username"].toString();
	config.password = cfg["password"].toString();
	config.dataBaseName = cfg["database"].toString();
	// ToDo: Write other Configs

	int position = listDcu.length();

	listDcu.append(DataControlUnit(new GridFileSystemController(config, position), true));
//	DataControlUnit iDcu = DataControlUnit(new GridFileSystemController(config, position), true);
	GridFileSystemController* x = listDcu[position].ctrlr;
	// ToDo: Make Connections
	connect(this, &AsyncBinFileReader::initFSController, this, [x, position](int pos)
	{
		if (pos == position)
		{
			x->init();
		}
	});
	connect(this, &AsyncBinFileReader::getBinaryData, this, [x, position](int pos, ObjectIdType oid, GridFileSystemController::BinaryReceiveOptions REC_Option = GridFileSystemController::REC_UseByteCount, GridFileSystemController::BinaryFetchOptions BIN_Option = GridFileSystemController::BIN_All, int BIN_ObjectSeqNo = 0, int BIN_NumberOfObjects = 1)
	{
		if (pos == position)
		{
			x->getBinaryData(oid, REC_Option, BIN_Option, BIN_ObjectSeqNo, BIN_NumberOfObjects);
		}
	});

	connect(x, &GridFileSystemController::binaryData, this, &AsyncBinFileReader::receiveBinaryData);

	emit initFSController(position);
	return position;
}
