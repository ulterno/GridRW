#ifndef ASYNCBINFILEREADER_H
#define ASYNCBINFILEREADER_H

#include <QObject>
#include <QThread>
#include <QJsonDocument>
#include <QBuffer>
#include "GridFileSystemController.h"

class AsyncBinFileReader : public QObject
{
	Q_OBJECT

public:
	explicit AsyncBinFileReader(QJsonDocument config, QObject *parent = nullptr);
	int binaryDataQuery (ObjectIdType oid);

signals:

	void initFSController (int workerSeqNumber);
	void getBinaryData (int position, ObjectIdType oid, GridFileSystemController::BinaryReceiveOptions REC_Option = GridFileSystemController::REC_UseByteCount, GridFileSystemController::BinaryFetchOptions BIN_Option = GridFileSystemController::BIN_All, int BIN_ObjectSeqNo = 0, int BIN_NumberOfObjects = 1);
	void dataReady (QSharedPointer<QBuffer> dataBuffer, int fileSeqNo);

public slots:
	void receiveFileMetadata (QJsonDocument metadata, bool success, int workerSeqNumber);
	void receiveBinaryData (QByteArray data, bool over, int workerSeqNumber);

private:
	GridFileSystemController::DB_CONFIG config;
	struct DataControlUnit
	{
		bool inUse{};
		GridFileSystemController *ctrlr = nullptr;
		QByteArray *data = nullptr;
		///
		/// \brief fsThread : Perhaps change to QThreadPool implementation?
		///
		QThread *fsThread;
		DataControlUnit (GridFileSystemController* controllerObject, bool makeLocked)
			: ctrlr (controllerObject),
			data (new QByteArray)
		{
			fsThread = new QThread;
			ctrlr->moveToThread(fsThread);
			fsThread->start();
		}
		~DataControlUnit ()
		{
			ctrlr->abort = true;
			delete ctrlr;
			fsThread->quit();
			fsThread->wait();
			delete data;
			delete fsThread;
		}
	};

	QList<DataControlUnit> listDcu;

	int addController (const QJsonDocument &config);
};

#endif // ASYNCBINFILEREADER_H
