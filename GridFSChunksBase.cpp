#include "GridFSChunksBase.h"

GRIDFS_CPP_REG(Base64BinData)
GRIDFS_CPP_REG(BinData)
GRIDFS_CPP_REG(GridFSChunksBase)

namespace qx
{
	template <> void register_class (QxClass<Base64BinData> &t)
	{
		t.data(&Base64BinData::m_base64, "base64");
		t.data(&Base64BinData::m_subType, "subType");
	}
	template <> void register_class (QxClass<BinData> &t)
	{
		t.data(&BinData::dlr_binary, "$binary");
	}
	template <> void register_class (QxClass<GridFSChunksBase> &t)
	{
		t.id(&GridFSChunksBase::m_id, "_id");

		t.data(&GridFSChunksBase::m_files_id, "files_id");
		t.data(&GridFSChunksBase::m_n, "n");
		t.data(&GridFSChunksBase::m_data, "data");
	}
}
