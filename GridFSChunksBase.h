#ifndef GRIDFSCHUNKSBASE_H
#define GRIDFSCHUNKSBASE_H

#include "gridfs_common.h"
#include <QVariant>
#include <QDateTime>

class GRIDFS_DLL_EXP Base64BinData
{
public:

	QByteArray m_base64;
	QString m_subType;

	Base64BinData (){};
	virtual ~Base64BinData(){};
};

GRIDFS_HPP_NOBASE_REG(Base64BinData)

class GRIDFS_DLL_EXP BinData
{
public:

	Base64BinData dlr_binary;

	BinData(){};
	virtual ~BinData(){};
};

GRIDFS_HPP_NOBASE_REG(BinData)

class GRIDFS_DLL_EXP GridFSChunksBase
{
public:

	ObjectIdType m_id;
	ObjectIdType m_files_id;
	qint32 m_n = 1000;
	BinData m_data{};

	QString getId () const
	{
		return m_id["$oid"];
	}
	QString getFiles_id () const
	{
		return m_files_id["$oid"];
	}
	QByteArray getBinaryData () const
	{
		return m_data.dlr_binary.m_base64;
	}
	QString getDataType () const
	{
		return m_data.dlr_binary.m_subType;
	}
	GridFSChunksBase(){};
	virtual ~GridFSChunksBase(){};
};

GRIDFS_PRIMARY_KEY_OBJID(GridFSChunksBase)
GRIDFS_HPP_NOBASE_REG(GridFSChunksBase)

#endif // GRIDFSCHUNKSBASE_H
