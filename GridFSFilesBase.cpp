#include "GridFSFilesBase.h"

GRIDFS_CPP_REG(GridFSFilesBase)

namespace qx
{
	template <> void register_class (QxClass<GridFSFilesBase> &t)
	{
		t.id(&GridFSFilesBase::m_id, "_id");

		t.data(&GridFSFilesBase::m_length, "length");
		t.data(&GridFSFilesBase::m_chunkSize, "chunkSize");
		t.data(&GridFSFilesBase::m_uploadDate, "uploadDate");
		t.data(&GridFSFilesBase::m_filename, "filename");
		t.data(&GridFSFilesBase::m_aliases, "aliases");
		t.data(&GridFSFilesBase::m_metadata, "metadata");

		t.data(&GridFSFilesBase::m_md5, "md5");
		t.data(&GridFSFilesBase::m_contentType, "contentType");
	}
}
