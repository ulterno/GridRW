#ifndef GRIDFSFILESBASE_H
#define GRIDFSFILESBASE_H

#include "gridfs_common.h"
#include <QVariant>
#include <QDateTime>

class GRIDFS_DLL_EXP GridFSFilesBase
{
public:

	ObjectIdType m_id;
	qint64 m_length {};
	qint32 m_chunkSize {};
	QDateTime m_uploadDate;
	QString m_filename;
	QStringList m_aliases;
	QVariant m_metadata;

	/// \deprecated m_md5
	QString m_md5;
	/// \deprecated m_contentType
	QString m_contentType;

	QString getId () const
	{
		return m_id["$oid"];
	}

	GridFSFilesBase(){};
	virtual ~GridFSFilesBase(){};
};

GRIDFS_PRIMARY_KEY_OBJID (GridFSFilesBase)
GRIDFS_HPP_NOBASE_REG (GridFSFilesBase)

#endif // GRIDFSFILESBASE_H
