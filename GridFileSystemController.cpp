#include "GridFileSystemController.h"

#include <QtMath>
#include <QMutexLocker>

GridFileSystemController::GridFileSystemController(const DB_CONFIG &configs, int workerSeqNumber)
	: QObject {nullptr}, selfSeqNumber {workerSeqNumber}, config {configs}
{}

void GridFileSystemController::init()
{
	ndb = qx::QxSqlDatabase::getSingleton();
	ndb->setDriverName("QXMONGODB", true);
	ndb->setHostName(config.hostName, true);
	ndb->setPort(config.portNo, true);
	ndb->setDatabaseName(config.dataBaseName, true);
	ndb->setUserName(config.username, true);
	ndb->setPassword(config.password, true);
	inited = true;
}

void GridFileSystemController::getFileMetaData(QString filename, FileFetchOptions option, int numberOfDocs, int startingPoint)
{
	busy = true;
	if (!retInited() || abort)
	{
		busy = false;
		return;
	}
	QList<UserGridFSFileClass> fileInfoList;
	if (!fetchFileInfo(fileInfoList, filename))
	{
		emit fileMetadata(QJsonDocument{}, false, selfSeqNumber);
		busy = false;
		return;
	}
	switch (option)
	{
	case FI_All:
	{
		emit fileMetadata(QJsonDocument::fromJson(qx::serialization::json::to_byte_array(fileInfoList)), true, selfSeqNumber);
		break;
	}
	case FI_First:
	{
		emit fileMetadata(QJsonDocument::fromJson(qx::serialization::json::to_byte_array(fileInfoList.at(0))), true, selfSeqNumber);
		break;
	}
	case FI_SpecifiedNumber:
	{
		emit fileMetadata(QJsonDocument::fromJson(qx::serialization::json::to_byte_array(fileInfoList.mid(startingPoint, numberOfDocs))), true, selfSeqNumber);
		break;
	}
	}
	busy = false;
}

void GridFileSystemController::getFileMetaData(const ObjectIdType &oid)
{
	busy = true;
	if (!retInited() || abort)
	{
		busy = false;
		return;
	}
	UserGridFSFileClass fileInfo;
	if (!fetchFileInfo(fileInfo, oid))
	{
		emit fileMetadata(QJsonDocument{}, false, selfSeqNumber);
		busy = false;
		return;
	}
	emit fileMetadata(QJsonDocument::fromJson(qx::serialization::json::to_byte_array(fileInfo)), true, selfSeqNumber);
	busy = false;
}

void GridFileSystemController::getBinaryData(QString filename, BinaryReceiveOptions REC_Option, BinaryFetchOptions BIN_Option, int BIN_ObjectSeqNo, int BIN_NumberOfObjects, FileFetchOptions FI_Option, int FI_NumberOfDocs, int FI_StartingPoint)
{
	busy = true;
	if (!retInited() || abort)
	{
		busy = false;
		return;
	}
	QList<UserGridFSFileClass> fileInfoList;
	if (!fetchFileInfo(fileInfoList, filename))
	{
		emit fileMetadata(QJsonDocument{}, false, selfSeqNumber);
		busy = false;
		return;
	}
	switch (REC_Option)
	{
	case REC_AllTogether:
	{
		QString errStr = "getBinaryData(QString filename, ...) case REC_AllTogether not implemented";
		emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
		break;
	}
	case REC_OneByOne:
	{
		for (auto fileInfo : fileInfoList)
		{
			if (abort)
			{
				busy = false;
				return;
			}
			qint32 numberOfChunks = qCeil(qreal(fileInfo.m_length)/ qreal(fileInfo.m_chunkSize));
			for (int iter = 0; iter < numberOfChunks; ++iter)
			{
				QList<UserGridFSChunksClass> binDataList;
				if (!fetchBinaryData(binDataList, fileInfo.m_id, iter))
				{
					QString errStr = "Failed getting Chunk.. \n FileOID:" + fileInfo.getId() + "; SerNo: " + QString::number(iter);
					emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
				}
				else
				{
					emit binaryData(binDataList.at(0).getBinaryData(), false, selfSeqNumber);
				}
			}
			emit binaryData (QByteArray{}, true, selfSeqNumber);
		}
		emit binaryData (QByteArray{}, true, selfSeqNumber);
		break;
	}
	case REC_UseByteCount:
	{
		QString errStr = "getBinaryData(QString filename, ...) case REC_UseByteCount not implemented";
		emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
		break;
	}
	}

	busy = false;
}

void GridFileSystemController::getBinaryData(ObjectIdType oid, BinaryReceiveOptions REC_Option, BinaryFetchOptions BIN_Option, int BIN_ObjectSeqNo, int BIN_NumberOfObjects)
{
	busy = true;
	if (!retInited() || abort)
	{
		busy = false;
		return;
	}
	QList<UserGridFSChunksClass> binDataList;
	///	Get Binary Data
	switch (BIN_Option)
	{
	case BIN_SpecifiedNumber:
	{
		if (fetchBinaryData(binDataList, oid, BIN_ObjectSeqNo, BIN_NumberOfObjects, BIN_SpecifiedNumber))
		{
			if (binDataList.length() == BIN_NumberOfObjects)
			{
				break; // Success
			}
			else
			{
				QString errStr = "Failed getting all required chunks in FileInfo.. \n FileOID:" + oid["$oid"] + "\n Required : " + QString::number(BIN_NumberOfObjects) + " Got: " + QString::number(binDataList.length());
				emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
				busy = false;
				return;
			}
		}
		else
		{
			QString errStr = "Failed getting Chunk from.. \n FileOID:" + oid["$oid"] ;
			emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
			busy = false;
			return;
		}
		break;
	}
	case BIN_All:
	{

		UserGridFSFileClass fileInfo;
		if (!fetchFileInfo(fileInfo, oid))
		{
			QString errStr = "Failed getting FileInfo.. \n FileOID:" + fileInfo.getId();
			emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
			busy = false;
			return;
		}
		qint32 numberOfChunks = qCeil(qreal(fileInfo.m_length)/ qreal(fileInfo.m_chunkSize));

		if (fetchBinaryData(binDataList, oid, 0, numberOfChunks, BIN_SpecifiedNumber))
		{
			if (binDataList.length() == numberOfChunks)
			{
				break; // Success
			}
			else
			{
				QString errStr = "Failed getting all required chunks in FileInfo.. \n FileOID:" + fileInfo.getId() + "\n Required : " + QString::number(numberOfChunks) + " Got: " + QString::number(binDataList.length());
				emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
				busy = false;
				return;
			}
		}
		else
		{
			QString errStr = "Failed getting Chunk from.. \n FileOID:" + fileInfo.getId() ;
			emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
			busy = false;
			return;
		}
		break;
	}
	}
	///	Emit Binary Data
	switch (REC_Option)
	{
	case REC_AllTogether:
	{
		QByteArray dataToEmit;
		for (auto xdf : binDataList)
		{
			dataToEmit.append(xdf.getBinaryData());
		}
		emit binaryData(dataToEmit, true, selfSeqNumber);
		break;
	}
	case REC_OneByOne:
	{
		for (auto xdf : binDataList)
		{
			emit binaryData(xdf.getBinaryData(), false, selfSeqNumber);
		}
		emit binaryData(QByteArray{}, true, selfSeqNumber);
		break;
	}
	case REC_UseByteCount:
	{
		QByteArray dataToEmit;
		int bindatalength = binDataList.at(0).getBinaryData().length();
		for (auto xdf : binDataList)
		{
			if ((dataToEmit.length() + bindatalength) >= config.byteSizeLimitHint)
			{
				emit binaryData(dataToEmit, false, selfSeqNumber);
				dataToEmit.clear();
			}
			dataToEmit.append(xdf.getBinaryData());
		}
		emit binaryData(dataToEmit, true, selfSeqNumber);
		break;
	}
	default:
		Q_ASSERT(false);
		QString errStr = "getBinaryData(ObjectIdType oid, ...) ERROR!! Default Case reached";
		emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
		break;
	}
	busy = false;
}

bool GridFileSystemController::retInited()
{
	busy = true;
	if (!inited)
	{
		emit notInitialised(selfSeqNumber);
	}
	busy = false;
	return inited;
}

bool GridFileSystemController::fetchFileInfo(QList<UserGridFSFileClass> &data, QString filename)
{
	QString savedClassName = qx::QxClassName<UserGridFSFileClass>::get();
	QSqlError err;
	{
		QMutexLocker lockingtheclass (&UserGridFSFileClass::accessControl);
		auto cc = qx::QxClassX::getClass(savedClassName);
//		cc->setName(config.filesCollName);
		err = qx::dao::fetch_by_query(qx_query ("{\"filename\" : \""+ filename +"\"}"), data);
		cc->setName(savedClassName);
	}
	if (err.isValid())
	{
		emit error (err, selfSeqNumber);
		qDebug () << err.databaseText();
		return false;
	}
	if (data.length() == 0)
	{
		qDebug () << "No data for filename: " << filename << "in " << config.filesCollName;
		return false;
	}
	return true;
}

bool GridFileSystemController::fetchFileInfo(UserGridFSFileClass &data, ObjectIdType oid)
{
	QString savedClassName = qx::QxClassName<UserGridFSFileClass>::get();
	QSqlError err;
	{
		QMutexLocker lockingtheclass (&UserGridFSFileClass::accessControl);
		auto cc = qx::QxClassX::getClass(savedClassName);
//		cc->setName(config.filesCollName);
		data.m_id = oid;
		err = qx::dao::fetch_by_id(data);
//		cc->setName(savedClassName);
	}
	if (err.isValid())
	{
		emit error (err, selfSeqNumber);
		qDebug () << err.databaseText();
		return false;
	}
	if (data.m_length == 0)
	{
		qDebug () << "No data for file: " << oid["$oid"] << " in " << config.filesCollName;
		return false;
	}
	return true;
}

bool GridFileSystemController::fetchBinaryData(QList<UserGridFSChunksClass> &data, QString filename, int BIN_ObjectSeqNo, int BIN_NumberOfObjects, BinaryFetchOptions BIN_Option, FileFetchOptions FI_Option, int FI_NumberOfDocs, int FI_StartingPoint)
{
	Q_ASSERT (false);
}

bool GridFileSystemController::fetchBinaryData(QList<UserGridFSChunksClass> &data, const ObjectIdType &oid, int BIN_ObjectSeqNo, int BIN_NumberOfObjects, BinaryFetchOptions BIN_Option)
{
	Q_ASSERT (BIN_Option != BIN_All);
	switch (BIN_Option)
	{
	case BIN_SpecifiedNumber:
	{
		bool retVal = false;
		for (int iter = 0; iter < BIN_NumberOfObjects ; ++iter)
		{
			if (abort)
			{
				break;
			}
			QList<UserGridFSChunksClass> binData;
			qx_query chunkyAggr ("cursor", "{\"aggregate\" : \"fs.chunks\" , \"pipeline\" : [{\"$match\" : {\"files_id\" : {\"$in\" : [{\"$oid\" : \"" + oid["$oid"] + "\"}]}, \"n\" : " + QString::number(BIN_ObjectSeqNo + iter) + " }}], \"cursor\" : {}}");
			QSqlError err = qx::dao::call_query(chunkyAggr);
			if (err.isValid())
			{
				emit error (err, selfSeqNumber);
				return false;
			}
			bool serialSuccess = (bool)qx::serialization::json::from_byte_array(binData, chunkyAggr.response().toString().toUtf8());
			if (!serialSuccess)
			{
				QString errStr;
				if (chunkyAggr.response().toString().isEmpty())
				{
					errStr = "Empty Document";
				}
				else
				{
					errStr = "Problem with Serialisation";
				}
				emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
				return false;
			}
			data.append(binData);
			retVal = serialSuccess;
		}
		return retVal;
	}
	case BIN_All:
	{
		QString errStr = "fetchBinaryData(QList<UserGridFSChunksClass> &data, const ObjectIdType &oid, ...) Cases not implemented";
		emit error (QSqlError(errStr, errStr, QSqlError::UnknownError, errStr), selfSeqNumber);
	}
	default:
		return false;
	}
}
