#ifndef GRIDFILESYSTEMCONTROLLER_H
#define GRIDFILESYSTEMCONTROLLER_H

#include <QObject>
#include <QJsonDocument>
#include "UserGridFSFileClass.h"
#include "UserGridFSChunksClass.h"

class GridFileSystemController : public QObject
{
	Q_OBJECT
public:

	int selfSeqNumber = 0;

	/**
	 * @brief abort : Set this to true if the operation is taking too long and you want to cancel it
	 * The next operation won't work until you reset this variable
	 * @note : Replace this with a QMutex once all logic is set
	 */
	bool abort = false;

	/**
	 * @brief busy : to check if an operation is undergoing
	 */
	bool busy = false;

	struct DB_CONFIG
	{
		QString driverName = "QXMONGODB";
		QString hostName = "localhost";
		int portNo = 27017;
		QString dataBaseName = "err_no_db_defined";
		QString username{};
		QString password{};
		QString filesCollName = "fs.files";
		QString chunksCollName = "fs.chunks";
		void setBucketName (QString name)
		{
			filesCollName = name + ".file";
			chunksCollName = name + ".chunks";
		}
		qint64 byteSizeLimitHint = 20000000;
	} config;

	enum FileFetchOptions {FI_First, FI_All, FI_SpecifiedNumber};
	enum BinaryFetchOptions {BIN_All, BIN_SpecifiedNumber};
	/**
	 * @brief The BinaryReceiveOptions enum tells how many binary chunks are to be concatenated before emitting
	 */
	enum BinaryReceiveOptions {REC_OneByOne, REC_AllTogether, REC_UseByteCount};

	explicit GridFileSystemController(const DB_CONFIG &config, int workerSeqNumber);

signals:

	void error (QSqlError err, int workerSeqNumber);

	void notInitialised (int workerSeqNumber);

	void fileMetadata (QJsonDocument metadata, bool success, int workerSeqNumber);

	void binaryData (QByteArray data, bool over, int workerSeqNumber);

public slots:

	void init ();

	void getFileMetaData (QString filename, FileFetchOptions option = FI_First, int numberOfDocs = 1, int startingPoint = 0);
	void getFileMetaData (const ObjectIdType &oid);

	void getBinaryData (QString filename, BinaryReceiveOptions REC_Option = REC_UseByteCount, BinaryFetchOptions BIN_Option = BIN_All, int BIN_ObjectSeqNo = 0, int BIN_NumberOfObjects = 1, FileFetchOptions FI_Option = FI_First, int FI_NumberOfDocs = 1, int FI_StartingPoint = 0);
	///
	/// \brief getBinaryData : Default option has been implemented and ready for testing
	/// \param oid
	/// \param REC_Option
	/// \param BIN_Option
	/// \param BIN_ObjectSeqNo
	/// \param BIN_NumberOfObjects
	///
	void getBinaryData (ObjectIdType oid, BinaryReceiveOptions REC_Option = REC_UseByteCount, BinaryFetchOptions BIN_Option = BIN_All, int BIN_ObjectSeqNo = 0, int BIN_NumberOfObjects = 1);

private:
	qx::QxSqlDatabase *ndb;

	bool retInited ();
	bool inited {};

	bool fetchFileInfo (QList<UserGridFSFileClass> &data, QString filename);
	bool fetchFileInfo (UserGridFSFileClass &data, ObjectIdType oid);

	bool fetchBinaryData (QList<UserGridFSChunksClass> &data, QString filename, int BIN_ObjectSeqNo = 0, int BIN_NumberOfObjects = 1, BinaryFetchOptions BIN_Option = BIN_SpecifiedNumber, FileFetchOptions FI_Option = FI_First, int FI_NumberOfDocs = 1, int FI_StartingPoint = 0);
	bool fetchBinaryData (QList<UserGridFSChunksClass> &data, const ObjectIdType &oid, int BIN_ObjectSeqNo = 0, int BIN_NumberOfObjects = 1, BinaryFetchOptions BIN_Option = BIN_SpecifiedNumber);
};

#endif // GRIDFILESYSTEMCONTROLLER_H
