#include "SimpleAsyncBinFileReader.h"

SimpleAsyncBinFileReader::SimpleAsyncBinFileReader(QJsonDocument cfg, QObject *parent)
	: QObject{parent}
{
	GridFileSystemController::DB_CONFIG config{};
	config.hostName = cfg["host"].toString();
	config.portNo = cfg["port"].toInt();
	config.username = cfg["username"].toString();
	config.password = cfg["password"].toString();
	config.dataBaseName = cfg["database"].toString();

	ctrlr = new GridFileSystemController (config, 0);
	ctrlr->moveToThread(&fsThread);
	fsThread.start();

	connect(this, &SimpleAsyncBinFileReader::initFSController, ctrlr, [this](int pos)
	{
		Q_UNUSED(pos);
		ctrlr->init();
	});
	connect(this, &SimpleAsyncBinFileReader::getBinaryData, ctrlr, [this](int pos, ObjectIdType oid, GridFileSystemController::BinaryReceiveOptions REC_Option = GridFileSystemController::REC_UseByteCount, GridFileSystemController::BinaryFetchOptions BIN_Option = GridFileSystemController::BIN_All, int BIN_ObjectSeqNo = 0, int BIN_NumberOfObjects = 1)
	{
		Q_UNUSED(pos);
		ctrlr->getBinaryData(oid, REC_Option, BIN_Option, BIN_ObjectSeqNo, BIN_NumberOfObjects);
	});

	connect(ctrlr, &GridFileSystemController::binaryData, this, &SimpleAsyncBinFileReader::receiveBinaryData);

	emit initFSController(0);
}

int SimpleAsyncBinFileReader::binaryDataQuery(ObjectIdType oid)
{
	emit getBinaryData(0, oid);
	return 0;
}

void SimpleAsyncBinFileReader::receiveBinaryData(QByteArray data, bool over, int workerSeqNumber)
{
	qDebug () << "void SimpleAsyncBinFileReader::receiveBinaryData(QByteArray data, bool over, int workerSeqNumber)";
	recData.append(data);
	if (over)
	{
		qDebug () << "RecData length: " << recData.length();
//		QBuffer aBuffer (new QBuffer (&recData));
		emit dataReady(recData, workerSeqNumber);
	}
}
