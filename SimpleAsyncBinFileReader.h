#ifndef SIMPLEASYNCBINFILEREADER_H
#define SIMPLEASYNCBINFILEREADER_H

#include <QObject>
#include <QThread>
#include <QJsonDocument>
#include <QBuffer>
#include "GridFileSystemController.h"

class SimpleAsyncBinFileReader : public QObject
{
	Q_OBJECT
public:
	explicit SimpleAsyncBinFileReader(QJsonDocument config, QObject *parent = nullptr);
	int binaryDataQuery (ObjectIdType oid);

	void receiveBinaryData (QByteArray data, bool over, int workerSeqNumber);

signals:
	void initFSController (int workerSeqNumber);
	void getBinaryData (int position, ObjectIdType oid, GridFileSystemController::BinaryReceiveOptions REC_Option = GridFileSystemController::REC_UseByteCount, GridFileSystemController::BinaryFetchOptions BIN_Option = GridFileSystemController::BIN_All, int BIN_ObjectSeqNo = 0, int BIN_NumberOfObjects = 1);
	void dataReady (QByteArray &dataBuffer, int fileSeqNo);

private:
	GridFileSystemController::DB_CONFIG config;

	GridFileSystemController *ctrlr = nullptr;
	QByteArray recData;
	QThread fsThread;

};

#endif // SIMPLEASYNCBINFILEREADER_H
