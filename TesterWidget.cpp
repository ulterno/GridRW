#include "TesterWidget.h"
#include "ui_TesterWidget.h"

TesterWidget::TesterWidget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::TesterWidget)
{
	ui->setupUi(this);

	QFile conFile (":/gridfs_conn_config.json");
	conFile.open(QFile::ReadOnly);
	QJsonParseError err;
	QJsonDocument confDoc = QJsonDocument::fromJson(conFile.readAll(), &err);
	conFile.close();
	if (err.error != QJsonParseError::NoError)
	{
		qDebug () << "Config File Error: " << err.errorString();
	}
	QJsonObject config = confDoc.object();
//	ndb = qx::QxSqlDatabase::getSingleton();
//	ndb->setDriverName("QXMONGODB");
//	ndb->setHostName(config["host"].toString());
//	ndb->setPort(config["port"].toInt());
//	ndb->setDatabaseName(config["database"].toString());
//	ndb->setUserName(config["username"].toString());
//	ndb->setPassword(config["password"].toString());

	nr = new SimpleAsyncBinFileReader (confDoc);
}

TesterWidget::~TesterWidget()
{
	delete ui;
}


void TesterWidget::on_pB_GetGridFSData_clicked()
{
	QList<UserGridFSFileClass> fileInfo;
	QSqlError err = qx::dao::fetch_by_query(qx_query ("{\"filename\" : \""+ui->lE_filename->text()+"\"}"), fileInfo);
	if (err.isValid())
	{
		qDebug () << err.databaseText();
		return;
	}
	if (fileInfo.length() == 0)
	{
		ui->textBrowser->append("File Not Found");
		return;
	}
	int counter = 1;
	for (auto fI : fileInfo)
	{
		ui->textBrowser->append("_____________________________________________________\n" + QString::number(counter));
		ui->textBrowser->append(qx::serialization::json::to_string(fI));
	}
	auto fI = fileInfo.at(0);
	QList<UserGridFSChunksClass> fileData{};
	int iterations = qCeil(qreal(fI.m_length)/qreal(fI.m_chunkSize));
	bool serialSuccess = true;
	for (int iter = 0; iter < iterations ; iter ++)
	{
		//	err = qx::dao::fetch_by_query(qx_query ("{\"files_id.oid\" : \""+ getOidString(fI.m_id) +"\"}"), fileData);
		qx_query chunkyAggr ("cursor", "{\"aggregate\" : \"fs.chunks\" , \"pipeline\" : [{\"$match\" : {\"files_id\" : {\"$in\" : [{\"$oid\" : \"" + fI.m_id["$oid"] + "\"}]}, \"n\" :  " + QString::number(iter) + " }}], \"cursor\" : {}}");
		qDebug () << chunkyAggr.query();
		err = qx::dao::call_query(chunkyAggr);
		if (err.isValid())
		{
			qDebug () << err.databaseText();
			return;
		}
		//	qDebug () << "___________________RETURNDATA____________________\n"
		//			  << QString::fromUtf8(QJsonDocument::fromJson(chunkyAggr.response().toString().toUtf8()).toJson());
		QList<UserGridFSChunksClass> infd;
		serialSuccess = serialSuccess ? (bool)qx::serialization::json::from_byte_array(infd, chunkyAggr.response().toString().toUtf8()) : false;
		if (serialSuccess)
		{
			fileData.append(infd);
		}
	}
	qDebug () << "Serial Success : " << serialSuccess;
	ui->textBrowser->append("______________________DATA_________________________");
	QByteArray BinaryData;
	if (fileData.length() == 0)
	{
		ui->textBrowser->append("Unable to get data objects");
		return;
	}
	ui->textBrowser->append("Number of Chunks: " + QString::number(fileData.length()));
	for (auto fD : fileData)
	{
		ui->textBrowser->append("Chunk:- ID: " + fD.m_files_id["$oid"] + " \tSer: " + QString::number(fD.m_n) + "\tObjId: " + fD.m_id["$oid"]);
		BinaryData.append(fD.m_data.dlr_binary.m_base64);
	}
	QImage aI;
	if (aI.loadFromData(BinaryData))
	{
		QPixmap pI;
		pI.convertFromImage(aI);
		ui->label->setPixmap(pI);
	}
	else
	{
		ui->label->setText(QString::fromUtf8(BinaryData));
		QFile wF (fI.m_filename);
		wF.open(QFile::WriteOnly);
		wF.write(BinaryData);
		wF.close();
	}
}

void TesterWidget::on_pB_OtherStuff_clicked()
{
	UserGridFSChunksClass d;
	ui->label->setText(getQxClassName(d));
}

void TesterWidget::fa(GridFSFilesBase *c)
{
	qx::dao::fetch_all(c);
}


template<class G>
QString TesterWidget::getQxClassName(G &var)
{
//	qx::dao::fetch_all(var);
	return QString (qx::QxClassName<G>::get());
}

void TesterWidget::on_pB_fa_clicked()
{
	UserGridFSFileClass x;
	fa(&x);
}


void TesterWidget::on_pB_get2_clicked()
{
	ObjectIdType oid; oid.insert("$oid", ui->lE_filename->text());
	connect (nr, &SimpleAsyncBinFileReader::dataReady, this, [this] (QByteArray& dataBuffer, int fileSeqNo)
	{
		qDebug () << "TesterWidget::lambda";
		Q_UNUSED(fileSeqNo);
		QFile wF (ui->lE_filename->text());
		wF.open(QFile::WriteOnly);
			wF.write(dataBuffer);
		wF.close();
	});
	nr->binaryDataQuery(oid);
}

