#ifndef TESTERWIDGET_H
#define TESTERWIDGET_H

#include <QWidget>
#include <QFile>
#include <QTextStream>
#include <QtMath>
#include <SimpleAsyncBinFileReader.h>

QT_BEGIN_NAMESPACE
namespace Ui { class TesterWidget; }
QT_END_NAMESPACE

class TesterWidget : public QWidget
{
	Q_OBJECT

public:
	TesterWidget(QWidget *parent = nullptr);
	~TesterWidget();

private slots:
	void on_pB_GetGridFSData_clicked();

	void on_pB_OtherStuff_clicked();

	void on_pB_fa_clicked();

	void on_pB_get2_clicked();

private:
	Ui::TesterWidget *ui;
	qx::QxSqlDatabase *ndb;

	QString getOidString (QString m_id);
	template <class G> QString getQxClassName (G &var);
	void fa(GridFSFilesBase *c);


	SimpleAsyncBinFileReader *nr;
};
#endif // TESTERWIDGET_H
