#include "UserGridFSChunksClass.h"

GRIDFS_CPP_REG(UserGridFSChunksClass)

namespace qx
{
	template <> void register_class (QxClass<UserGridFSChunksClass> &t)
	{
		t.setName("fs.chunks");
	}
}
