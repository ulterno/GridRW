#ifndef USERGRIDFSCHUNKSCLASS_H
#define USERGRIDFSCHUNKSCLASS_H

#include "GridFSChunksBase.h"
#include <QMutex>

class GRIDFS_DLL_EXP UserGridFSChunksClass : public GridFSChunksBase
{
public:
	/**
	 * @brief accessControl : Use this whenever you change the registered name of this class
	 * Change it back BEFORE unlocking the mutex
	 */
	static QMutex accessControl;

	UserGridFSChunksClass(){};
	virtual ~UserGridFSChunksClass(){};
};

GRIDFS_HPP_REG(UserGridFSChunksClass, GridFSChunksBase, 0)

#endif // USERGRIDFSCHUNKSCLASS_H
