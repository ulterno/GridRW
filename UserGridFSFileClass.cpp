#include "UserGridFSFileClass.h"

GRIDFS_CPP_REG(UserGridFSFileClass)

namespace qx
{
	template <> void register_class (QxClass<UserGridFSFileClass> &t)
	{
		t.setName("fs.files");
	}
}

QMutex UserGridFSFileClass::accessControl;
