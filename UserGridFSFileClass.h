#ifndef USERGRIDFSFILECLASS_H
#define USERGRIDFSFILECLASS_H

#include <QMutex>
#include "GridFSFilesBase.h"

class GRIDFS_DLL_EXP UserGridFSFileClass : public GridFSFilesBase
{
public:

	static QMutex accessControl;

	UserGridFSFileClass(){};
	virtual ~UserGridFSFileClass(){};
};

GRIDFS_HPP_REG(UserGridFSFileClass, GridFSFilesBase, 0);

#endif // USERGRIDFSFILECLASS_H
