QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

QX_BSON_INCLUDE_PATH=/opt/mongo/drivers/mongoc/include/libbson-1.0/
QX_BSON_LIB_PATH=/opt/mongo/drivers/mongoc/lib/
QX_MONGOC_INCLUDE_PATH=/opt/mongo/drivers/mongoc/include/libmongoc-1.0/
QX_MONGOC_LIB_PATH=/opt/mongo/drivers/mongoc/lib/

include($$PWD/../../../QxOrm/QxOrm/QxOrm.pri)

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x051507    # disables all the APIs deprecated before Qt 5.15.7

SOURCES += \
    AsyncBinFileReader.cpp \
    GridFSChunksBase.cpp \
    GridFSFilesBase.cpp \
    GridFileSystemController.cpp \
    SimpleAsyncBinFileReader.cpp \
    UserGridFSChunksClass.cpp \
    UserGridFSFileClass.cpp \
    main.cpp \
    TesterWidget.cpp

HEADERS += \
    AsyncBinFileReader.h \
    GridFSChunksBase.h \
    GridFSFilesBase.h \
    GridFileSystemController.h \
    SimpleAsyncBinFileReader.h \
    TesterWidget.h \
    UserGridFSChunksClass.h \
    UserGridFSFileClass.h \
    gridfs_common.h

FORMS += \
    TesterWidget.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../QxOrm/QxOrm/lib/ -lQxOrm
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../QxOrm/QxOrm/lib/ -lQxOrmd
else:unix: LIBS += -L$$PWD/../../../QxOrm/QxOrm/lib/ -lQxOrm

INCLUDEPATH += $$PWD/../../../QxOrm/QxOrm/include
DEPENDPATH += $$PWD/../../../QxOrm/QxOrm/include

DISTFILES +=

RESOURCES += \
    res.qrc
