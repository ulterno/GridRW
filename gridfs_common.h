#ifndef GRIDFS_COMMON_H
#define GRIDFS_COMMON_H

#include <QxOrm.h>

#define GRIDFS_DLL_EXP									QX_DLL_EXPORT_HELPER
#define GRIDFS_HPP_REG									QX_REGISTER_HPP_EXPORT_DLL
#define GRIDFS_HPP_NOBASE_REG(className)				QX_REGISTER_HPP_EXPORT_DLL(className, qx::trait::no_base_class_defined, 0)
#define GRIDFS_CPP_REG									QX_REGISTER_CPP_EXPORT_DLL

#define GRIDFS_PRIMARY_KEY_TYPE							QX_REGISTER_PRIMARY_KEY
#define GRIDFS_PRIMARY_KEY_QSTRING(className)			QX_REGISTER_PRIMARY_KEY(className, QString)

typedef QMap<QString, QString> ObjectIdType;

//ObjectIdType makeObjectId (const QString &idValue)
//{
//	ObjectIdType ret;
//	ret.insert("$oid", idValue);
//	return ret;
//}
#define GRIDFS_PRIMARY_KEY_OBJID(className)			QX_REGISTER_PRIMARY_KEY(className, ObjectIdType)

#endif // GRIDFS_COMMON_H
